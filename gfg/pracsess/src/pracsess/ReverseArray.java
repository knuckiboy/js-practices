package pracsess;

import java.util.List;

public class ReverseArray {
	
	private static int[] reverseArr(int[] arr, int n, int k) {
		int remain = n%k;
		int div = n/k;
		
		for(int i=0;i<k*div;i+=k) {
//		System.out.println(i+" "+(i+k-1));
			
			reverseArrImpl(arr,i,i+k-1);
		}
		if(remain!=0) {
			if(remain!=1) {
				for(int j=n-remain;j<n;j+=remain) {
					reverseArrImpl(arr,j,n-1);
				}				
			}
		}
		return arr;
	}
	private static int[] reverseArrImpl(int[] arr, int l, int r) {
		int e = l+(r-l)/2;
		if(r-l%2!=0) {
			e+=1;
		}
		for(int i=l;i<e;i++) {
			int temp = arr[i];
			arr[i] = arr[r-i+l];
			arr[r-i+l] = temp;
		}
		return arr;
	}
	
	public static void main(String[] args) {
		int[] y = new int[] {7,2,3,4,5,6,1,7,4};
//		int[] y = new int[] {1,2,3,4,5};
		
		int[] x = reverseArr(y, 9, 4);
		for(int i=0;i<x.length;i++) {
			System.out.println(x[i]);
		}
		
	}

}
