/**
 * 
 */
package pracsess;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * @author ngjiannan
 *
 */
public class RecursiveDups {
	
	 static String remove(String s) {
		 // code here
		 Stack<Character> listChar = new Stack<>();
		 String result = "";
		 
		 char[] chrs = s.toCharArray();
		 int i= 1;
		 listChar.add(chrs[0]);
		 	while(i<chrs.length) {
		 		if(chrs[i-1]!=chrs[i]) {
		 				listChar.push(chrs[i]);
		 		}else if(!listChar.isEmpty() && listChar.peek()==chrs[i-1]) {
		 				listChar.pop();
		 			}
		 		i++;
		 	}
		 	for(Character c: listChar) {
		 		result+=c;
		 	}
		 	return result;
	    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(remove("abccbccba"));
	}

}
