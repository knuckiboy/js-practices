class Solution {
    maxSumIS(arr,n){
        //code here
        let arr1=[], arr2=[]
        
        for(let i=0;i<n;i++){
            arr1.push(1)
            arr2.push(arr[0])
        }
      
        let j = 0;
        let k = 1;
        let max = arr[0];
        let omax = arr2[0]
        while(k<n){
            j=0;
            max = 0
            while(j<k){
                if(arr[k]>arr[j] && max<arr2[j]){
                   max = arr2[j]
                }
                j++

                if(max === 0){
                    arr2[k] = arr[k]
                }else{
                    arr2[k] = max+arr[k]
                }
                omax = Math.max.apply(null,[omax,arr2[k]]);
            }
            k++
        }

        return Math.max.apply(null,arr2)
    }
}

var soln = new Solution();

console.log(soln.maxSumIS([44, 42, 38, 21, 15 ,22 ,13 ,27],8));