

class Solution 
{
    //Function to check if a string can be obtained by rotating
    //another string by exactly 2 places.
     isRotated(str1, str2)
    {
        // code here
        return (String(str1).slice(2).concat(String(str1).slice(0,2))==str2|| String(str1).slice(String(str1).length-2).concat(String(str1).slice(0,String(str1).length-2))==str2)?1:0
    }
}

var soln = new Solution();

console.log(soln.isRotated("wlrbbmqbhcdar", "owkkyhiddqscd"));