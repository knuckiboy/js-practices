function isPalindrome(str) {
  let arr = [],
    j = 0,
    str2 = "";
  for (let i = str.length - 1; i >= 0; i--) {
    arr[j] = str.charAt(i);
    j++;
  }
  console.log(arr);

  for (let i = 0; i < str.length; i++) {
    str2 += arr[i];
  }

  return str === str2;
}

console.log(isPalindrome("eey"));

function binarySearch(arr, l, r, target) {
  if (r < l) return -1;

  let m = l + (r - l) / 2;

  if (arr[m] === target) return m;
  else if (arr[m] > target) {
    binarySearch(arr, m + 1, r, target);
  } else if (arr[m] < target) {
    binarySearch(arr, l, m, target);
  }
}

function matchNoteToMag(n, m) {
  let map1 = new Map(),
    map2 = new Map();
  for (num of n) {
    if (map1.has(num)) {
      map1.set(num, map1.get() + 1);
    } else {
      map1.set(num, 1);
    }
  }

  for (num of m) {
    if (map2.has(num)) {
      map2.set(num, map2.get() + 1);
    } else {
      map2.set(num, 1);
    }
  }

  for (let [k, v] of map1.entries) {
    if (!map2.has(k) || (map2.has(k) && v > map2.get(k))) {
      return false;
    }
  }
  return true;
}

function romanToInt(romStr) {
  let intStr = 0,
    i = 0,
    le = romStr.length;

  while (i < le ) {
    if (rCharToInt(romStr.charAt(i)) < rCharToInt(romStr.charAt(i + 1))) {
      intStr += -rCharToInt(romStr.charAt(i));
    } else {
      intStr += rCharToInt(romStr.charAt(i));
    }
    console.log(intStr)
    i++;
  }
  return intStr;
}

function rCharToInt(c) {
  switch (c) {
    case "I":
      return 1;
    case "V":
      return 5;
    case "X":
      return 10;
    case "L":
      return 50;
    case "C":
      return 100;
    case "D":
      return 500;
    case "M":
      return 1000;
  }
  return 0;
}

console.log(romanToInt("XLIX"));
