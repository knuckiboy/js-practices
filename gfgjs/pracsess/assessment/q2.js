class Solution {
    solution(N){
       
        let string = N.toString()
        let maxVal = 0
        if(string.startsWith("-")){
            maxVal = parseInt(String(string).substring(0,1)+"5"+String(string).substring(1))
        }else{
            maxVal = parseInt("5"+string)
        }
        let i =string.startsWith("-")? 2:1
        while(i<string.length){
        let val = String(string).substring(0,i)+"5"+String(string).substring(i);
            if(maxVal<parseInt(val)){
                maxVal = parseInt(val)
            }
            i++
        }
        return maxVal
    }
  }
  
  var soln = new Solution();
  
  console.log(soln.solution(-5999));
  