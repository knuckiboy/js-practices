class Solution{
    removeDups(str){
        //code here
      return Array.from(new Set(str)).join("")
    }
}

var soln = new Solution();

console.log(soln.removeDups("zvvo"));