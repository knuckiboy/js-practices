
class Solution {
    longestCommonSubstr(S1,S2,n,m){
        //code here
        let dj = []
        let max = 0;
        for(let i=0;i<n+1;i++){
            let arr1 =[]
            for(let j=0;j<m+1;j++){
                arr1.push(0)
            }
            dj.push(arr1)
        };
        let a = 1; 
        while(a<=n){
            let b = 1;
            while(b<=m){
                if(S1.charAt(a-1)===S2.charAt(b-1)){
                    dj[a][b] = dj[a-1][b-1]+1;
                    if(dj[a][b]>max){
                        max = dj[a][b];
                    }
                }
                b++;
            }

            a++;
        }
        
        return max
    }
}

var soln = new Solution();

  soln.longestCommonSubstr("ABC","ACB", 3, 3);