class Solution {
  mergeSort(A, l, r) {
    if (l >= r) {
      return;
    }
    let m = l + parseInt((r - l) / 2);
    this.mergeSort(A, l, m);
    this.mergeSort(A, m + 1, r);
    this.merge(A, l, m, r);
  }

  merge(A, l, m, r) {
    let a = [],
      b = [],
      i = 0,
      j = 0;

    for (let k = 0; k < m - l + 1; k++) {
      a.push(A[l + k]);
    }
    for (let p = 0; p < r - m; p++) {
      b.push(A[p + m + 1]);
    }

    console.log(a + " " + b);
    let d = l;
    while (i < (m - l + 1) && j < r - m) {
      if (a[i] <= b[j]) {
        A[d] = a[i];
        i++;
      } else {
        A[d] = b[j];
        j++;
      }
      d++;
    }

    while (i < (m - l + 1)) {
      A[d] = a[i];
      d++;
      i++;
    }
    while (j < r - m) {
      A[d] = b[j];
      d++;
      j++;
    }
  }

  solution(A, k) {
    A.sort((a,b)=> a-b)
    // this.mergeSort(A, 0, A.length);
    console.log(A);
    return A[k - 1];
  }
}

var soln = new Solution();

console.log(soln.solution([7, 10, 4, 3, 20, 15], 4));
