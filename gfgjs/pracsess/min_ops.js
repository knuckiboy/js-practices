
class Solution{

    minOperation(n){
        //code here
        var counter =0

        while (n>1){
            if(n%2!=0){
                counter++
                n=n-1
            }
                n=n/2
                counter++;
        }
         return counter+1
    }

}



var soln = new Solution();

console.log(soln.minOperation(8));