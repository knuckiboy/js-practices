class Solution {
  maxChainLen(arr, n) {
    //code here
    arr.sort(function(a, b){ return (a[1]-b[1])})
    let count = 0;
    let p1 = arr[0][1]
    var i ,j
    i=1;
    while(i<n){
            if(arr[i][0]> p1){
                p1 = arr[i][1]
                count++
            }
        i++;
    }
    return count+1
  }
}

var soln = new Solution();

console.log(
//   soln.maxChainLen([[5, 24], [39, 60], [15, 28], [27, 40], [50, 90]], 5)
  soln.maxChainLen([[5, 10], [1,11]], 2)
);
