/*
 * Complete the 'workbook' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER k
 *  3. INTEGER_ARRAY arr
 */

function workbook(n, k, arr) {
  // Write your code here
  let pages = 0,
    count = 0;
  for (let v of arr) {
    let fp = 0;
    let lp = 0;
    let j = 1;
    if (Math.floor(v / k) != 0) {
      while (j <= Math.floor(v / k)) {
        pages += 1;
        fp = 1 + k * (j - 1);
        lp = fp + (k - 1);
        // console.log(Math.floor(v / k) + " "+ v + " "+k + " "+ j);
        // console.log(fp + " " + lp + " " + pages + " ");
        if (pages >= fp && pages <= lp) {
          count++;
        }
        j++;
      }
    }
    if (v % k != 0) {
      fp = lp + 1;
      pages += 1;
      lp = fp - 1 + (v % k);
    //   console.log(fp + " " + lp + " " + pages + " ");
      if (pages >= fp && pages <= lp) {
        count++;
      }
    }
    // console.log(pages);
  }
  return count;
}

console.log(workbook(5, 3, [4, 2, 6, 1, 10]));
