/*
 * Complete the 'palindromeIndex' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function palindromeIndex(s) {
  // Write your code here
  let arr = Array.from(s),
    n = arr.length;
  if (s === reverse(s)) return -1;
  let index = 0;
  for (let i = 0; i < Math.floor(arr.length / 2); i++) {
    if (arr[i] != arr[n - i - 1]) {
      let temp = arr[i];
      arr[i] = "";
    //   console.log(arr.join("")+ " " + reverse(arr.join("")));
      if (arr.join("") == reverse(arr.join(""))) {
        index = i;
        break;
      } else {
        index = n - i - 1;
        break;
      }
      // console.log(arr)
      arr[i] = temp;
    }
  }

  return index;
}

function reverse(s) {
  return s.split("").reverse().join("");
}

// console.log(
//   palindromeIndex("hgygsvlfcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcwflvsgygh")
// );
console.log(
  palindromeIndex("baa")
);
