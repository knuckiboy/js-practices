// function largestPermutation(k, arr) {
//   // Write your code here
//   let sortedArr = [...arr].sort((a, b) => b-a);
//   for (let i = 0; i < k; i++) {
//       if (arr[i] != sortedArr[i]) {
//         // console.log(sortedArr[i])
//         // console.log(arr.indexOf(sortedArr[i]))
//       swap(arr, i, arr.indexOf(sortedArr[i]));
//     }
//   }
//   return arr;
// }

function largestPermutation(k, arr) {
  // Write your code here
  let record = {};
  for (let i = 0; i < arr.length; i++) {
    record[arr[i]] = i;
  }
//   console.log(record);
  let swaps = 0,
    j = 0;
  while (swaps < k && j < arr.length) {
    if (arr[j] < arr.length - j) {
      let idx = record[arr.length - j];
      swap(arr, idx, j);
      record[arr[idx]] = idx;
      record[arr.length - j] = j;
      swaps++;
    }
    j++;
  }
//   console.log(arr);
  return arr;
}

function swap(arr, a, b) {
  // console.log(a+" "+arr[a]);
  // console.log(b+" "+arr[b]);
  let temp = arr[b];
  arr[b] = arr[a];
  arr[a] = temp;
}

console.log(largestPermutation(1, [4, 2, 3, 5, 1]));
