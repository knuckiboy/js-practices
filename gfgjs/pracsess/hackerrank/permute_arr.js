function twoArrays(k, A, B) {
  // Write your code here
  A.sort((a, b) => a - b);
  B.sort((a, b) => b - a);

//   console.log(A);
//   console.log(B);

  for (let i = 0; i < A.length; i++) {
    // console.log(k - A[i] - B[i]);
    if (k - (A[i] + B[i]) > 0) {
      return "NO";
    }
  }
  return "YES";
}

console.log(twoArrays(5, [1, 2, 2, 1], [3, 3, 3, 4]));
