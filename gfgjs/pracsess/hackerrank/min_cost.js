class Solution {
    //Function to return max value that can be put in knapsack of capacity W.
    solveMeFirst(k, c) {
        c.sort((a,b)=> a-b)
        let sum =0
        let i= c.length-1-k
        sum = c.slice(c.length-k).reduce((a,b)=> a+b,0)
        console.log(c)
        let j=1,h=0
        while(i>=0){
            if(h==k){
                h=0;
                j++
            }
            sum = sum + (j+1)*c[i]
            console.log((j+1)*c[i])
            i--
            h++
        }
        return sum;
    }

}

var soln = new Solution();

// console.log(soln.solveMeFirst(3,[390225, 426456, 688267, 800389 ,990107, 439248, 240638 ,15991 ,874479, 568754 ,729927 ,980985 ,132244 ,488186, 5037 ,721765, 251885, 28458, 23710 ,281490 ,30935 ,897665, 768945, 337228, 533277 ,959855, 927447 ,941485, 24242, 684459, 312855, 716170, 512600, 608266, 779912 ,950103, 211756, 665028, 642996 ,262173 ,789020 ,932421, 390745, 433434 ,350262, 463568, 668809, 305781, 815771, 550800]));
console.log(soln.solveMeFirst(3,[1,3,5,7,9]));
