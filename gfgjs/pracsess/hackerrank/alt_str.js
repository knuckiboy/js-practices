/*
 * Complete the 'alternatingCharacters' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function alternatingCharacters(s) {
  // Write your code here
  let arr = [s[0]];
  let count = 0;
  let s1 = Array.from(s);
  for (let j = 1; j < s1.length; j++) {
    if (arr[arr.length - 1] != s1[j]) {
      arr.push(s1[j]);
    } else {
      count++;
    }
  }
  return count;
}

console.log(alternatingCharacters("AAABBB"));
