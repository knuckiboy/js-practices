/*
 * Complete the 'howManyGames' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER p
 *  2. INTEGER d
 *  3. INTEGER m
 *  4. INTEGER s
 */

function howManyGames(p, d, m, s) {
  // Return the number of games you can buy
  let c = 0,
    i = 0;
  while (c <= s) {
    let price = p - d * i > m ? p - d * i : m;
    c += price;
    console.log(c + " " + i);
    if (c <= s) {
      i++;
    }
  }
  return i;
}

console.log(howManyGames(16, 2, 1, 9981));
