class Solution {
  //Function to return max value that can be put in knapsack of capacity W.
  contribution_height(current, previous) {
    return Math.abs(current - previous);
  }

  solveMeFirst(A) {
    let N = A.length;
    let M = A[0].length;
    let ans = 0;

    // Traversing the matrix.
    for (let i = 0; i < N; i++) {
      for (let j = 0; j < M; j++) {
        /* If we are traveling the topmost
                row in the matrix, we declare the
                wall above it as 0 as there is no
                wall above it. */
        let up = 0;

        /* If we are traveling the leftmost
                column in the matrix, we declare the
                wall left to it as 0as there is no
                wall left it. */
        let left = 0;

        // If its not the topmost row
        if (i > 0) up = A[i - 1][j];

        // If its not the leftmost column
        if (j > 0) left = A[i][j - 1];

        // Summing up the contribution of by
        // the current block
        ans +=
        Math.abs(A[i][j]- up) + Math.abs(A[i][j]- left);

        /* If its the rightmost block of the matrix
                it will contribute area equal to its height
                as a wall on the right of the figure */
        if (i == N - 1) ans += A[i][j];

        /* If its the lowest block of the
                matrix it will contribute area equal
                to its height as a wall on
                 the bottom of the figure */
        if (j == M - 1) ans += A[i][j];
      }
    }

    // Adding the contribution by
    // the base and top of the figure
    ans += N * M * 2;
    return ans;
  }
}

var soln = new Solution();

console.log(
  soln.solveMeFirst([
    [1, 3, 4],
    [2, 2, 3],
    [1, 2, 4],
  ])
    // soln.solveMeFirst([[51, 32, 28, 49, 28, 21, 98, 56, 99, 77]])
  //   soln.solveMeFirst([[1]])
);
