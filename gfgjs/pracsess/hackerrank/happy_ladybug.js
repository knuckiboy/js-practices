/*
 * Complete the 'happyLadybugs' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING b as parameter.
 */

function happyLadybugs(b) {
  // Write your code here
  let o = new Map();

  for (let c of b) {
    if (o.has(c)) {
      o.set(c, o.get(c) + 1);
    } else {
      o.set(c, 1);
    }
  }
  //   console.log(o);

  let arr = Array.from(b);

  for (let [k, v] of o.entries()) {
    if (v == 1 && k != "_") return "NO";
  }
  if ((o.has("_") && o.get("_") > 0)) {
    return "YES";
  } else {
    let pair = 0;

    for (let i = 0; i < arr.length; i++) {
      if (arr[i] == arr[i + 1]) {
        pair++;
      } else if (pair > 0) {
        pair = 0;
      } else {
        return "NO";
      }
    }
  }
  return "YES";
}

console.log(happyLadybugs("AABCBC"));
