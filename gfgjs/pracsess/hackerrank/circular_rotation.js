function circularArrayRotation(a, k, queries) {
  let res = [],
    n = a.length;
  k = k % n; // k becomes from 0 to n

  //   console.log(queries)
  for (let num in queries) {
    res.push(a[(n - k + num) % n]); // n-k will give starting point of changed index + q to find index, % n ot avoid out of bounds
  }
  return res;
}

console.log(
  circularArrayRotation(
    [
      596, 53804, 13567, 538, 55791, 75204, 15873, 98117, 41061, 49377, 64425,
      62232, 49127, 20342, 99276, 24860, 25787, 81841, 97359, 7404, 34400,
      37174, 73359, 257, 88477, 28020, 25920, 62652, 3228, 20401, 99722, 20175,
      74204, 29640, 20713, 29994, 4843, 52937, 28110, 45903, 18665, 92535,
      24487, 67791, 29228, 23762, 9002, 71367, 21955, 6360, 78770, 56354, 59886,
      68480, 72962, 48362, 96499, 15233, 11013, 99726, 35633, 27086, 36253,
      9836, 73077, 56965, 39830, 77919, 26253, 84291, 40174, 44918,
    ],
    3,
    [0, 2, 1]
  )
);
