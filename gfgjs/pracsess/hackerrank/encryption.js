class Solution {
  //Function to return max value that can be put in knapsack of capacity W.
  solveMeFirst(s) {
    s = String(s).replaceAll(" ", "");
    let col = Math.ceil(Math.sqrt(s.length));
    let row = Math.floor(Math.sqrt(s.length));
    let i = 0,
      j = 0,
      k = 0,
      result = "";
    // console.log(s)
    if(col*row<s.length){
        row = col
    }

    while (i < col) {
      j = i;
      console.log(i + col * (row-1))
      while (j <= i + col * (row-1)) {
        if(s[j]!==undefined){
            result += s[j];
        }
        j = j + col;
      }
      result += " ";
      i++;
    }

    return result;
  }
}

var soln = new Solution();

console.log(soln.solveMeFirst("chillout"));
