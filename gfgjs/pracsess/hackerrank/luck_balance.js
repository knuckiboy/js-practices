function luckBalance(k, contests) {
  // Write your code here
  let row = contests.length;
  let sum = 0,
    j = 0;
  contests.sort((a, b) => b[0] - a[0]);

//   console.log(contests)


  for (let i = 0; i < row; i++) {
    if (contests[i][1] == 0) {
        // console.log(contests[i][0])
      sum += contests[i][0];
    }else{
        if(j<k){
            // console.log(contests[i][0])
            sum += contests[i][0];
            j++
        }else{
            sum -= contests[i][0];
        }
    }
  }

  return sum
}

// console.log(
//   luckBalance(3, [
//     [5, 1],
//     [2, 1],
//     [1, 1],
//     [8, 1],
//     [10, 0],
//     [5, 0],
//   ])
// );
console.log(
  luckBalance(1, [
    [5, 1],
    [1, 1],
    [4, 0],
  ])
);
