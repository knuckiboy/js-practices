function jimOrders(orders) {
  // Write your code here
  let data = {},
    row = orders.length;

  for (let i = 0; i < row; i++) {
    console.log(orders[i][0] + " " + orders[i][1]);
    data[i + 1] = orders[i][0] + orders[i][1];
  }

  let sorted = Object.keys(data).sort((a, b) => (data[a] - data[b])).map(a => parseInt(a));
  return sorted
}

// console.log(
//   jimOrders([
//     [8, 1],
//     [4, 2],
//     [5, 6],
//     [3, 1],
//     [4, 3],
//   ])
// );
console.log(
  jimOrders([
    [1, 3],
    [2, 3],
    [3, 3],
  ])
);
