function minimumNumber(n, password) {
  // Return the minimum number of characters to make the password strong
  let numbers = "0123456789";
  let lower_case = "abcdefghijklmnopqrstuvwxyz";
  let upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let special_characters = "!@#$%^&*()-+";
  let regex = /[a-z]/;
  let regex2 = /[A-Z]/;
  let regex3 = /[0-9]/;
  let regex4 = /[!@#$%^&*()+-]/;
  let count = 0;

  if (!regex.test(password)) {
    count++;
    console.log("a"+count);
  }
  if (!regex2.test(password)) {
    count++;
    console.log("b"+count);
  }
  if (!regex3.test(password)) {
    count++;
    console.log("c"+count);
  }
  if (!regex4.test(password)) {
    count++;
    console.log("d"+count);
  }
  console.log(count);
  if (password.length < 6) {
    // console.log(6 - (password.length + count));
    count +=
      6 - (password.length + count) < 0 ? 0 : 6 - (password.length + count);
  }

  return count;
}

console.log(minimumNumber(7, "AUzs-nV"));
