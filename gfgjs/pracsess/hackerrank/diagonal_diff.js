function diagonalDifference(arr) {
  // Write your code here
  let ltr = 0;
  let rtl = 0;
  let row = arr.length,
    col = arr[0].length;
  for (let j = 0; j < row; j++) {
    ltr += arr[j][j];
    rtl += arr[row - j - 1][j];
    // console.log(arr[j][j]);
    // console.log(arr[row - j - 1][j]);
  }
  return Math.abs(ltr - rtl);
}

console.log(
  diagonalDifference([
    [1, 2, 3],
    [4, 5, 6],
    [9, 8, 9],
  ])
);
