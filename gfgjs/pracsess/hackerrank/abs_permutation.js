/*
 * Complete the 'absolutePermutation' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER k
 */

function absolutePermutation(n, k) {
  // Write your code here
  let arr = Array(n)
    .fill(0)
    .map((e, i) => i + 1);

  if (k == 0) {
    return arr;
  }

  if (n % 2 == 1) {
    return [-1];
  }

  for (let i = 0; i < arr.length - k; i++) {
    let index = arr.indexOf(arr[i] + k);
    if (arr[i] == arr[i + k] - k) {
      [arr[i], arr[index]] = [arr[index], arr[i]];
      // console.log(i + " " + index);
      console.log(arr);
    } else if (Math.abs(arr[i] - (i + 1)) != k) {
      return [-1];
    }
  }

  for (let i = 0; i < arr.length; i++) {
    //   console.log(arr[i] - (i + 1))
    if (Math.abs(arr[i] - (i + 1)) != k) {
      return [-1];
    }
  }

  return arr;
}

console.log(absolutePermutation(10, 5));
