// Complete the flatlandSpaceStations function below.
function flatlandSpaceStations(n, c) {
  let max = 0;
  let diff = Array(n)
    .fill(0)
    .map((a, i) => i)
    .filter((x) => !c.includes(x));

  if (diff.length == 0) {
    return 0;
  }

  for (let j = 0; j < diff.length; j++) {
    let minDist = Number.MAX_SAFE_INTEGER;
    let i = 0;
    while (i < c.length) {
      let dist = Math.abs(diff[j] - c[i]);
    //   console.log(dist);
      minDist = Math.min(dist, minDist);
    //   console.log(minDist);
      i++;
    }
    max = Math.max(max, minDist);
  }

//   console.log(max);
  return max;
}

console.log(flatlandSpaceStations(6, [0, 1, 2, 3, 4, 5]));
