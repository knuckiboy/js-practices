function connectedCell(matrix) {
  // Write your code here
  let max = 0;
  let row = matrix.length;
  let col = matrix[0].length;

  for (let i = 0; i < row; i++) {
    for (let j = 0; j < col; j++) {
      //   console.log(i + " " + j);
      if (matrix[i][j] == 1) {
        max = Math.max(dfs(matrix, i, j), max);
      }
    }
  }
  return max;
}

function dfs(matrix, i, j) {
  if (i < 0 || j < 0 || i >= matrix.length || j >= matrix[0].length) return 0;

  if (matrix[i][j] == 0) return 0;

  let sum = 1;
  matrix[i][j] = 0;

  for (let r = i - 1; r <= i + 1; r++) {
    for (let c = j - 1; c <= j + 1; c++) {
      if (r != i || c != j) {
        sum += dfs(matrix, r, c);
      }
    }
  }
  //   console.log(sum);
  return sum;
}

console.log(
  connectedCell([
    [0, 0, 1, 1],
    [0, 0, 1, 0],
    [0, 1, 1, 0],
    [0, 1, 0, 0],
    [1, 1, 0, 0],
  ])
);
