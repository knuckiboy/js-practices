class Solution {
  //Function to return max value that can be put in knapsack of capacity W.
  solveMeFirst(a) {
    // code here
    let i = 0,
      max = 0,
      min = Number.MAX_VALUE;
    let total = a.reduce((c, d) => c + d, 0);
    while (i < a.length) {
      if (total - a[i] > max) {
        max = total - a[i];
      }
      if (total - a[i] < min) {
        min = total - a[i];
      }

      i++;
    }
    console.log(min + " " + max);
  }

  sockMerchant(n, arr) {
    // code here
    let i = 0,
      j = 0,
      count = 0;
    while (i < n) {
      j = i + 1;
      while (j < n) {
        if (arr[i] !=-1 && arr[i] === arr[j]) {
          arr[i] = -1;
          arr[j] = -1;
          count++
          break
        }
        j++;
      }

      i++;
    }
    
    return count;
  }
}

var soln = new Solution();

console.log(soln.sockMerchant(10, [1, 1, 3, 1, 2, 1, 3, 3, 3, 3]));
