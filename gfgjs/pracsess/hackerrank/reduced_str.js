/*
 * Complete the 'superReducedString' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING s as parameter.
 */

function superReducedString(s) {
  // Write your code here
  let n = s.length,
    c = [s[0]];
  for (let i = 1; i < n; i++) {
    if (c[c.length - 1] == s[i]) {
      c.pop();
    } else {
      c.push(s[i]);
    }
  }
  //   console.log(c);
  return c.length == 0 ? "Empty String" : c.join("");
}

console.log(superReducedString("aa"));
