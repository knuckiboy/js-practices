/*
 * Complete the 'stringConstruction' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function stringConstruction(s) {
  // Write your code here
  let str = [];
  let count = 0;
  for (let i of s) {
    if (str.indexOf(i) == -1) {
      str[count] = i;
      count++;
    }
  }
//   console.log(str)
  return count
}

console.log(stringConstruction("abab"));
