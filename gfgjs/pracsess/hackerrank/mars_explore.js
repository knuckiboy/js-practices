/*
 * Complete the 'marsExploration' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function marsExploration(s) {
  // Write your code here
  let n = s.length,
    size = Math.floor(n / 3),
    sosStr = "SOS".repeat(size),
    count = 0;
  for (let i = 0; i < n; i++) {
    if (s[i] != sosStr[i]) {
      count++;
    }
  }
  return count;
}

console.log(marsExploration("SOSSOT"));
