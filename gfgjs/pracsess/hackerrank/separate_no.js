/*
 * Complete the 'separateNumbers' function below.
 *
 * The function accepts STRING s as parameter.
 */

function separateNumbers(s) {
  // Write your code here
  if (s.length === 1) console.log("NO");

  let firstVal = "",
    intVal = 0n,;
  for (let i = 0; i <= s.length / 2; i++) {
    firstVal = firstVal + s.charAt(i);
    intVal = +firstVal;
    let tempVal = firstVal;
    while (tempVal.length < s.length) {
      intVal = intVal + 1n;
      tempVal += intVal;
    }
    if (tempVal === s) {
      console.log("YES " + firstVal);
      return;
    }
  }
  console.log("NO");
  return;
}

console.log(separateNumbers("90071992547409929007199254740993"));
