class Solution {
  //Function to return max value that can be put in knapsack of capacity W.
  solve(X, N) {
    return this.findPowerSum(X,N,1,0)
  }

  findPowerSum(X,N,num,sum){
    if(sum===X){
      return 1
    }else{
      let noOfWays = 0
      if(sum<X){
        for(let i=num;i<=Math.pow(X,1/N);i++){
          noOfWays+=this.findPowerSum(X,N,i+1,sum+Math.pow(i,N))
          // console.log(noOfWays)
        }
      }
      return noOfWays
    }
  }
}

var soln = new Solution();

console.log(soln.solve(100, 2));
