function minimumAbsoluteDifference(arr) {
  // Write your code here
  let minDiff = Number.MAX_VALUE;

  arr.sort((a, b) => a - b);
  //   console.log(arr);
  for (let i = 1; i < arr.length; i++) {
      let diff = Math.abs(arr[i-1] - arr[i]);
      if (minDiff > diff) {
        minDiff = diff;
      }
    }
  return minDiff;
}

console.log(minimumAbsoluteDifference([1, -3, 71, 68, 17]));
