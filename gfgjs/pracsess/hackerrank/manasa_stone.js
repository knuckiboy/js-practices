/*
 * Complete the 'stones' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER a
 *  3. INTEGER b
 */

function stones(n, a, b) {
  // Write your code here
  let set = new Set();
  for (let i = 0; i < n; i++) {
    let sum = a * i + (n - i - 1) * b;
    set.add(sum);
  }
  return [...set].sort((a, b) => a - b);
}

console.log(stones(4, 10, 100));
