/*
 * Complete the 'beautifulPairs' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY A
 *  2. INTEGER_ARRAY B
 */

function beautifulPairs(A, B) {
  // Write your code here

  let m1 = new Map();
  let m2 = new Map();
  let c = 0;
  for (let i = 0; i < B.length; i++) {
    if (!m1.has(B[i])) {
      m1.set(B[i], 1);
    } else {
      m1.set(B[i], m1.get(B[i]) + 1);
    }
    if (!m2.has(A[i])) {
      m2.set(A[i], 1);
    } else {
      m2.set(A[i], m2.get(A[i]) + 1);
    }
  }

//   console.log(m2);
//   console.log(m1);

  for (let [k, v] of m2.entries()) {
    console.log(k + " " + v);
    if (m1.has(k)) {
      c += Math.min(v, m1.get(k));
    }
  }


  if (c === A.length) {
    c--;
  } else {
    c++;
  }
  return c;
}

console.log(beautifulPairs([1, 2, 3, 4], [1, 2, 3, 3]));
