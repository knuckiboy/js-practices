/*
 * Complete the 'pageCount' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER p
 */

function pageCount(n, p) {
  // Write your code here
  let pages = 0;
  let j = p == 1 ? 0 : 1;
  if (p == 1) return pages;
  if (p < (n + 1) / 2) {
    while (j < p) {
      pages++;
      j += 2;
      console.log(j + " " + pages);
    }
  } else {
    j = n % 2 == 0 ? n : n - 1;
    while (j > p) {
      j -= 2;
      // console.log(j);
      pages++;
    }
  }
  //   console.log(pages);
  return pages;
}

console.log(pageCount(5, 1));
