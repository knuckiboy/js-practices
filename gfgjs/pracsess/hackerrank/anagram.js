/*
 * Complete the 'anagram' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function anagram(s) {
  // Write your code here
  let n = s.length;
  if (n % 2 != 0) return -1;

  let s1 = s.substring(0, n / 2);
  let s2 = s.substring(n / 2);
  let s2arr = Array.from(s2);
//   console.log(s1 + " " + s2);
  let count = 0;
  for (let j = 0; j < s1.length; j++) {
    let index = s2arr.indexOf(s[j]);
    if (index == -1) {
      count++;
    } else {
        s2arr[index] = "";
    }
  }
    // console.log(s2arr);
  return count;
}

console.log(anagram("aaabbb"));
