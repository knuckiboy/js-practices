/*
 * Complete the 'twoStrings' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. STRING s1
 *  2. STRING s2
 */

function twoStrings(s1, s2) {
  // Write your code here
  let s1Obj = {},
    s2Obj = {};

  for (let j of s1) {
    if (j in s1Obj) {
      s1Obj[j] = s1Obj[j] + 1;
    } else {
      s1Obj[j] = 1;
    }
  }

  for (let i of s2) {
    if (i in s2Obj) {
      s2Obj[i] = s2Obj[i] + 1;
    } else {
      s2Obj[i] = 1;
    }
  }

  for (let k in s1Obj) {
    if (k in s2Obj) {
      return "YES";
    }
  }
  return "NO";
}

console.log(twoStrings("hi", "world"));
