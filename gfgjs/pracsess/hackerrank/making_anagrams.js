/*
 * Complete the 'makingAnagrams' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. STRING s1
 *  2. STRING s2
 */

function makingAnagrams(s1, s2) {
  // Write your code here
  let count = 0,
    arr2 = Array.from(s2);
  for (let i = 0; i < s1.length; i++) {
    let index = arr2.indexOf(s1[i]);
    if (index != -1) {
      count++;
      arr2[index] = "";
    } 
  }
//   console.log(arr2 + " " + count);
  return s1.length + s2.length - count * 2;
}

console.log(
  makingAnagrams(
    "absdjkvuahdakejfnfauhdsaavasdlkj",
    "djfladfhiawasdkjvalskufhafablsdkashlahdfa"
  )
);
