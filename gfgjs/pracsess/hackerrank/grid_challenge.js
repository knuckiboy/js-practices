function gridChallenge(grid) {
  // Write your code here
  let row = grid.length;
  let arr = [];
  for (let i = 0; i < row; i++) {
    arr.push(Array.from(grid[i]).sort());
  }
//   console.log(arr)

  for (let i = 0; i < arr[0].length; i++) {
    let a = [];
    for (let j = 0; j < arr.length; j++) {
      a.push(arr[j][i]);
    }
    if (!a.every((v, i, b) => !i || b[i - 1] <= v)) {
      return "NO";
    }
  }
  return "YES";
}

// console.log(gridChallenge(["ebacd", "fghij", "olmkn", "trpqs", "xywuv"]));
console.log(gridChallenge(["abc", "hjk", "mpq", "rtv"]));
