/*
 * Complete the 'gridSearch' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. STRING_ARRAY G
 *  2. STRING_ARRAY P
 */
function gridSearch(G, P) {
  // Write your code here
  let rowG = G.length,
    colG = G[0].length,
    rowP = P.length,
    colP = P[0].length;
  let arr = [];
  for (let i = 0; i < rowG; i++) {
    for (let j = 0; j < colG; j++) {
      if (G[i][j] == P[0][0]) {
        if (check(i, j, rowP, colP, rowG, colG, G, P)) {
          return "YES";
        }
      }
    }
  }
  return "NO";
}

function check(x, y, rowP, colP, rowG, colG, G, P) {
  for (let h = 0; h < colP; h++) {
    let k = 0;
    console.log(G[x + k][y + h] + " " + P[k][h]);
    while (x + k < rowG && y + h < colG && k < rowP) {
      if (G[x + k][y + h] != P[k][h]) {
        return false;
      }
      k++;
    }
  }
  return true;
}

// console.log(
//   gridSearch(
//     [
//       [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
//       [0, 9, 8, 7, 6, 5, 4, 3, 2, 1],
//       [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
//       [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
//       [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
//     ],
//     [
//       [8, 7, 6, 5, 4, 3],
//       [1, 1, 1, 1, 1, 1],
//       [1, 1, 1, 1, 1, 1],
//     ]
//   )
// );

console.log(
  gridSearch(
    [
      [4, 0, 0, 4, 5, 3, 5, 9, 2, 1, 2, 6, 5, 6, 0],
      [1, 1, 4, 2, 1, 3, 1, 3, 3, 0, 9, 8, 6, 9, 2],
      [4, 7, 4, 3, 8, 6, 0, 8, 2, 8, 7, 9, 6, 4, 8],
      [5, 2, 2, 3, 5, 6, 9, 5, 1, 1, 8, 9, 1, 6, 9],
      [8, 8, 7, 1, 0, 9, 4, 5, 0, 4, 8, 7, 4, 9, 6],
      [2, 5, 2, 8, 0, 2, 6, 3, 3, 3, 8, 8, 7, 8, 2],
      [5, 0, 2, 7, 7, 1, 4, 8, 4, 9, 6, 6, 7, 4, 8],
      [0, 7, 5, 9, 7, 5, 2, 0, 7, 6, 9, 3, 7, 8, 0],
      [5, 1, 1, 7, 9, 9, 7, 8, 9, 5, 6, 2, 8, 0, 6],
      [4, 0, 4, 0, 0, 7, 4, 5, 4, 2, 7, 2, 5, 0, 4],
      [5, 4, 9, 0, 4, 3, 8, 0, 9, 9, 1, 6, 0, 8, 0],
      [9, 6, 2, 4, 1, 0, 8, 0, 9, 5, 3, 4, 8, 1, 1],
      [4, 4, 5, 8, 9, 3, 5, 2, 3, 7, 3, 3, 4, 7, 5],
      [7, 6, 8, 7, 0, 5, 3, 0, 3, 2, 1, 4, 1, 7, 4],
      [6, 5, 0, 6, 2, 9, 2, 7, 0, 8, 8, 7, 1, 6, 0],
    ],
    [
      [9, 9],
      [9, 9],
    ]
  )
);
