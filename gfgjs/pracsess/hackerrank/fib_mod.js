/*
 * Complete the 'fibonacciModified' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER t1
 *  2. INTEGER t2
 *  3. INTEGER n
 */
let BigNumber = require("./bignumber.js");

BigNumber.config({ EXPONENTIAL_AT: 1e9 });
BigNumber.config({ POW_PRECISION: 0 });

function fibonacciModified(t1, t2, n) {
  // Write your code here
  let table = new Map();
  return fibonacciModifiedImpl(t1, t2, n, table);
}

function fibonacciModifiedImpl(t1, t2, n, table) {
  // Write your code here
  if (n == 1) {
    return t1;
  }

  if (n == 2) {
    return t2;
  }

  if (table.has(n)) {
    return table.get(n);
  } else {
    let val = 
      fibonacciModifiedImpl(t1, t2, n - 2, table) +
        fibonacciModifiedImpl(t1, t2, n - 1, table) ** 2
    table.set(n, val);
    return val;
  }
}

console.log(fibonacciModified(0, 1, 6));
