/*
 * Complete the 'larrysArray' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts INTEGER_ARRAY A as parameter.
 */

function larrysArray(A) {
  // Write your code here
  let sorted = Array(A.length)
    .fill(0)
    .map((a, i) => i + 1);

  for (let j = 0; j < A.length - 2; j++) {
    for (let i = j; i < A.length - 2; ) {
      if (A[i] === sorted[i]) {
        i++;
        continue;
      } else {
        if (swap(A, i, i + 1, i + 2, sorted)) {
          return "YES";
        } else {
          swap2(A, i, i + 1, i + 2, sorted);
        }
        i += 2;
      }
    }
  }
  return check(A, sorted);
}

// function larrysArray(A) {
//   // Write your code here
//   let s = 0;

//   for (let j = 0; j < A.length; j++) {
//     for (let i = j + 1; i < A.length; i++) {
//       if (A[j] > A[i]) {
//         s++;
//       }
//     }
//   }
//   return s % 2 == 0 ? "YES" : "NO";
// }

function check(A, sorted) {
  for (let k = 0; k < A.length; k++) {
    if (A[k] != sorted[k]) {
      return "NO";
    }
  }
  return "YES";
}

function swap(arr, a, b, c, sorted) {
  console.log(arr);
  [arr[a], arr[b], arr[c]] = [arr[b], arr[c], arr[a]];
    [arr[b], arr[c], arr[a]] = [arr[c], arr[a], arr[b]];
  console.log(arr);
  for (let k = 0; k < arr.length; k++) {
    if (arr[k] != sorted[k]) {
      return false;
    }
  }
  return true;
}

function swap2(arr, a, b, c, sorted) {
  console.log(arr);
  // [arr[a], arr[b], arr[c]] = [arr[b], arr[c], arr[a]];
  [arr[a], arr[b], arr[c]] = [arr[c], arr[a], arr[b]];
  console.log(arr);
  //   for (let k = 0; k < arr.length; k++) {
  //     if (arr[k] != sorted[k]) {
  //       return false;
  //     }
  //   }
  //   return true;
}

console.log(larrysArray([1, 6, 5, 2, 4, 3]));
// console.log(larrysArray([3, 1, 2]));
