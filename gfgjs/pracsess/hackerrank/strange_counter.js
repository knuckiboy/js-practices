/*
 * Complete the 'strangeCounter' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts LONG_INTEGER t as parameter.
 */

function strangeCounter2(t) {
  // Write your code here
  let st = 1,
    start = 3,
    value = start;

  while (st != t) {
    if (value == 1) {
      start *= 2;
      value = start;
    } else {
      value--;
    }
    st++;
  }
  return value;
}

function strangeCounter(t) {
    // Write your code here
    let n =3

    while(2*n-2 <=t){
        n*=2
    }
    return n-(t-(n-2))
  }

console.log(strangeCounter(13));
