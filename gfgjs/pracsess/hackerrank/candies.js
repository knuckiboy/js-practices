function candies(n, arr) {
  // Write your code here
  let arr2 = Array(n).fill(1);

  for (let i = 1; i < n; i++) {
    // console.log(arr[i+1]+" "+arr[i])
    if (arr[i] > arr[i - 1]) {
      arr2[i] = arr2[i-1] + 1;
    }
  }
  for (let i = n - 2; i >= 0; i--) {
    // console.log(arr[i - 1] + " " + arr[i]);
    if (arr[i] > arr[i + 1] && arr2[i] <= arr2[i + 1]) {
      arr2[i] = arr2[i + 1] + 1;
    }
  }

  // console.log(arr2)

  return arr2.reduce((a, b) => a + b, 0);
}

console.log(candies(10, [2, 4, 2, 6, 1, 7, 8, 9, 2, 1]));
