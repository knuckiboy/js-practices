function minimumLoss(price) {
  // Write your code here
  let min = Number.MAX_VALUE;

  let obj = {};
  for (let i = 0; i < price.length; i++) {
    obj[price[i]] = i;
  }
  price.sort((a, b) => a - b);
  console.log(price);
  for (let j = 1; j < price.length; j++) {
    if (obj[price[j]] < obj[price[j - 1]]) {
      let diff = price[j] - price[j - 1];
      if (min > diff) {
        min = diff;
      }
    }
  }
  return min;
}

console.log(minimumLoss([20, 7, 8, 2, 5]));
