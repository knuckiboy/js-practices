/*
 * Complete the 'gemstones' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING_ARRAY arr as parameter.
 */

function gemstones(arr) {
  // Write your code here
  let set = new Set();
  arr.sort((a, b) => a.length - b.length);
  let t = Array.from(arr[0]);
//   console.log(arr);
  for (let i = 0; i < t.length; i++) {
    let c = true;
    for (let j = 1; j < arr.length; j++) {
      if (arr[j].indexOf(t[i]) == -1) {
        c = false;
        break;
      }
    }
    if (c) {
        set.add(t[i])
    }
  }

//   console.log(set)
  return set.size
}

console.log(gemstones(["abc","abc","bc"]))
