function missingNumbers(arr, brr) {
  // Write your code here

  let crr = {};
  let drr = [];
  let i = 0;

  for (let i of brr) {
    crr[i] = (crr[i] ?? 0) + 1;
  }

  for (let j of arr) {
    crr[j] = crr[j] - 1;
  }

  for (let k in crr) {
    let d = 0;
    while (d < crr[k]) {
      drr.push(parseInt(k));
      d++;
    }
  }

  //   console.log(drr);

  //   console.log(arr);
  return drr;
}

// console.log(
//   missingNumbers(
//     [203, 204, 205, 206, 207, 208, 203, 204, 205, 206],
//     [203, 204, 204, 205, 206, 207, 205, 208, 203, 206, 205, 206, 204]
//   )
// );
console.log(missingNumbers([7, 2, 5, 3, 5, 3], [7, 2, 5, 4, 6, 3, 5, 3]));
