/*
 * Complete the 'cavityMap' function below.
 *
 * The function is expected to return a STRING_ARRAY.
 * The function accepts STRING_ARRAY grid as parameter.
 */

function cavityMap(grid) {
  // Write your code here
  let row = grid.length;
  for (let k = 0; k < row; k++) {
    grid[k] = grid[k].split("");
  }
//   console.log(grid);
  let col = grid[0].length;
  for (let i = 1; i < row - 1; i++) {
    for (let j = 1; j < col - 1; j++) {
      if (check(grid, i, j)) {
        grid[i][j] = "X";
      }
    }
  }
  for (let k = 0; k < row; k++) {
    grid[k] = grid[k].join("");
  }
//   console.log(grid);
  return grid;
}

function check(grid, r, c) {
  return (
    +grid[r][c] > +grid[r][c - 1] &&
    +grid[r][c] > +grid[r][c + 1] &&
    +grid[r][c] > +grid[r - 1][c] &&
    +grid[r][c] > +grid[r + 1][c]
  );
}

console.log(cavityMap(["1112", "1912", "1892", "1234"]));
