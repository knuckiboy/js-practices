/*
 * Complete the 'theLoveLetterMystery' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function theLoveLetterMystery(s) {
  // Write your code here
  let arr = Array.from(s),
    n = arr.length,
    count = 0;
  if (s == reverse(s)) return 0;

  for (let i = 0; i < n / 2; i++) {
    if (arr[i] != arr[n - i - 1]) {
      count += Math.abs(arr[i].charCodeAt(0) - arr[n - i - 1].charCodeAt(0));
    }
  }
  return count;
}

function reverse(s) {
  return s.split("").reverse().join("");
}

console.log(theLoveLetterMystery("abcd"));
