/*
 * Complete the 'angryProfessor' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. INTEGER k
 *  2. INTEGER_ARRAY a
 */

function angryProfessor(k, a) {
  a.sort((c, d) => c - d);
  //   console.log(a);
  let i = 0,
    b = a[i],
    count = 0;
  while (b <= 0) {
    count++;
    i++;
    b = a[i];
  }
  if (count >= k) {
    return "NO";
  }
  return "YES";
}

console.log(angryProfessor(2, [0, -1, 2, 1]));
