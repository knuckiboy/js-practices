/*
 * Complete the 'gamingArray' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function gamingArray(arr) {
  // Write your code here
  let i = arr.length,
    winner = "ANDY";
  while (i != 0) {
    let maxVal = Math.max(...arr);
    if (maxVal == 0) break;
    let index = arr.indexOf(maxVal);
    let j = index;

    for (; j < arr.length || arr[j] == 0; j++) {
      arr[j] = 0;
      //   console.log(arr);
    }
    i = index;
    winner = winner == "ANDY" ? "BOB" : "ANDY";
  }
  return winner;
}

console.log(gamingArray([5, 2, 6, 3, 4]));
