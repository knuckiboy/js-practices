function maximumToys(prices, k) {
  // Write your code here
  prices.sort((a, b) => a - b);
  let cost = 0,
    count = 0;
  for (let i = 0; i < prices.length; i++) {
    if (prices[i] < k) {
      cost += prices[i];
      if (k - cost > 0) {
        count++;
      } else {
        break;
      }
    }
  }
  return count;
}

console.log(maximumToys([1, 12, 5, 111, 200, 1000, 10], 50));
