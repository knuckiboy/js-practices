function sherlockAndAnagrams(s) {
  let map = getAllSubstrings(s);
  let count = 0;
  for (let [key, val] of map) {
    if (val > 1) {
      count = count + countPairs(val);
    }
  }
  return count;
}

function countPairs(n){
    return n*(n-1)/2
}

function findPairs(s, j, i) {
  if (s.length === 0 || s.length === 1 || i === s.length + 1) return 0;

  let sum = 0;
  for (let j = 0; j <= i; j++) {
    if (
      String(s).substring(j, i).length > 1 &&
      isAnagram(String(s).substring(j, i))
    ) {
      sum++;
    }
  }
  sum += findPairs(s, j, i + 1);
  return sum;
}
function getAllSubstrings(s) {
  const map = new Map();
  const n = s.length;
  for (let i = 0; i < n; ++i) {
    for (let j = i; j < n; ++j) {
      const sub = s.substring(i, j + 1);
      const key = sub.split("").sort().join("");
      if (map.has(key)) {
        map.set(key, map.get(key) + 1);
      } else {
        map.set(key, 1);
      }
    }
  } //done , subs
  // console.log(map)
  return map;
}

function isAnagram(s) {
  let arr = s.split("").reverse();
  return arr.join("") === s;
}

console.log(sherlockAndAnagrams("kkkk"));
