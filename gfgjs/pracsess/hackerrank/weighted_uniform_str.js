/*
 * Complete the 'weightedUniformStrings' function below.
 *
 * The function is expected to return a STRING_ARRAY.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. INTEGER_ARRAY queries
 */

function weightedUniformStrings(s, queries) {
    // Write your code here
    let obj = new Map(), sameCount = 1;
    obj.set(s[0], s[0].charCodeAt(0) - "a".charCodeAt(0) + 1);
  
    for (let i = 1; i < s.length; i++) {
      if (s[i] == s[i - 1]) {
        sameCount++;
        obj.set(
          s[i].repeat(sameCount),
          (s[i].charCodeAt(0) - "a".charCodeAt(0) + 1) * sameCount
        );
      } else {
        sameCount = 1;
        obj.set(s[i], (s[i].charCodeAt(0) - "a".charCodeAt(0) + 1) * sameCount);
      }
    }
    let vals = Array.from(obj.values());
    
    for (let j = 0; j < queries.length; j++) {
      if (vals.find((e) => e == queries[j])) {
        queries[j] = "Yes";
      }else{
        queries[j] = "No";
      }
    }
    return queries
  }

console.log(weightedUniformStrings("abbcccdddd", [1, 7, 5, 4, 15]));
