/*
 * Complete the 'organizingContainers' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts 2D_INTEGER_ARRAY container as parameter.
 * 
 */

function organizingContainers(container) {
  // Write your code here
  let row = container.length,
    colarr = Array(row).fill(0),
    rowarr = Array(row).fill(0);

  for (let i = 0; i < row; i++) {
    for (let j = 0; j < row; j++) {
      rowarr[i] += container[i][j];
      colarr[i] += container[j][i];
    }
  }
  console.log(rowarr);
  console.log(colarr);
  if (!compareArr(rowarr, colarr)) {
    return "Impossible";
  }

  return "Possible";
}

function compareArr(a, b) {
  a.sort((i1, i2) => i1 - i2);
  b.sort((i1, i2) => i1 - i2);

  for (let i = 0; i < a.length; i++) {
    if (a[i] != b[i]) {
      return false;
    }
  }
  return true;
}

console.log(
  organizingContainers([
    [0, 2, 1],
    [1, 1, 1],
    [2, 0, 0],
  ])
);
