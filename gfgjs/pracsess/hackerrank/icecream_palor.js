/*
 * Complete the 'icecreamParlor' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER m
 *  2. INTEGER_ARRAY arr
 */

function icecreamParlor(m, arr) {
  // Write your code here
  let arr2 = [];

  for (let i = 0; i < arr.length; i++) {
    let buy = arr[i];
    let index = arr.indexOf(m - buy);
    if (index != i && index != -1) {
      console.log(index);
      arr2.push(i + 1);
      arr2.push(index + 1);
      break;
    }
  }

  return arr2.sort((a, b) => a - b);
}

console.log(icecreamParlor(4, [1, 4, 5, 3, 2]));
