/*
 * Complete the 'caesarCipher' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. INTEGER k
 */

function caesarCipher(s, k) {
  // Write your code here
  let n = s.length;
  let t = Array.from(s);
  for (let j = 0; j < s.length; j++) {
    t[j] = String(t[j]).charCodeAt(0);
  }
  for (let i = 0; i < t.length; i++) {
    if (t[i] >= 65 && t[i] <= 90) {
      t[i] = 65 + ((t[i] + k - 65) % 26);
    } else if (t[i] >= 97 && t[i] <= 122) {
      t[i] = 97 + ((t[i] - 97 + k) % 26);
    }
  }

  return t.map((item) => String.fromCharCode(item)).join("");
}

console.log(caesarCipher("There's-a-starman-waiting-in-the-sky", 3));
