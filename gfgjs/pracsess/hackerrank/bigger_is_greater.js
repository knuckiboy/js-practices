class Solution {
  //Function to return max value that can be put in knapsack of capacity W.

  solveMeFirst(w) {
    let values = [...w];
  let point1 = values.length - 1;

  while (point1 > 0 && values[point1] <= values[point1 - 1]) point1--;

  if (!point1) return "no answer";

  let point2 = point1;


  point1--;

  let min = values[point2];


  for (let i = point1 + 2; i < values.length; i++) {
    values[i] < min &&
      values[i] > values[point1] &&
      ((point2 = i), (min = values[i]));
      console.log(min)
  }

  [values[point1], values[point2]] = [values[point2], values[point1]];

  return [
    ...values.slice(0, point1 + 1),
    ...values.slice(point1 + 1, values.length).sort((x, y) => (x >= y ? 1 : -1))
  ].join("");

  }
}

var soln = new Solution();

// console.log(soln.solveMeFirst(3,[390225, 426456, 688267, 800389 ,990107, 439248, 240638 ,15991 ,874479, 568754 ,729927 ,980985 ,132244 ,488186, 5037 ,721765, 251885, 28458, 23710 ,281490 ,30935 ,897665, 768945, 337228, 533277 ,959855, 927447 ,941485, 24242, 684459, 312855, 716170, 512600, 608266, 779912 ,950103, 211756, 665028, 642996 ,262173 ,789020 ,932421, 390745, 433434 ,350262, 463568, 668809, 305781, 815771, 550800]));
// console.log(soln.solveMeFirst("fedcbabcd"));
console.log(soln.solveMeFirst("fedcbabcd"));
