// Complete the jumpingOnClouds function below.
function jumpingOnClouds(c, k) {
  let i = 0,
    n = c.length,
    e = 100;
  while (true) {
    e = e - 1 - 2 * c[(i + k) % n];
    console.log(((i + k) % n) + " " + e);
    if ((i + k) % n == 0) {
      break;
    }
    i += k;
  }
  return e;
}

// console.log(jumpingOnClouds([0, 0, 1, 0, 0, 1, 1, 0], 2));
console.log(jumpingOnClouds([1, 1, 1, 0, 1, 1, 0, 0, 0, 0], 3));
