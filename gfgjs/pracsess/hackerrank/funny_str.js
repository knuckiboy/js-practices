function funnyString(s) {
  // Write your code here
  let revStr = s.split("").reverse().join("");

  let arr = Array.from(s).map((a, index) => String(a).charCodeAt());
  let revArr = Array.from(revStr).map((a, index) => String(a).charCodeAt());
  //   console.log(revArr);

  let max = 0;
  let arr1 = [];
  let arr2 = [];
  for (let i = 0; i < arr.length - 1; i++) {
    arr1.push(Math.abs(arr[i] - arr[i + 1]));
    arr2.push(Math.abs(revArr[i] - revArr[i + 1]));
  }

  let isFunny = 0;

  for (let k = 0; k < arr1.length; k++) {
    if (arr1[k] === arr2[k]) {
      isFunny++;
    } else {
      break;
    }
  }
  //   console.log(isFunny)
  if (isFunny == arr1.length) {
    return "Funny";
  } else {
    return "Not Funny";
  }
}

console.log(funnyString("acxz"));
