/*
 * Complete the 'kaprekarNumbers' function below.
 *
 * The function accepts following parameters:
 *  1. INTEGER p
 *  2. INTEGER q
 */

function kaprekarNumbers(p, q) {
  // Write your code here
  let i = p,
    list = [];
  while (i <= q) {
    if (isKaprekar(i)) {
      list.push(i);
    }
    i++;
  }
  if (list.length == 0) {
    console.log("INVALID RANGE");
  } else {
    console.log(list.reduce((a, b) => a + " " + b));
  }
  //   return list;
}

function isKaprekar(i) {
  let val = i ** 2,
    d = i.toString().length,
    j = val.toString().length;
  if (j > 1) {
    let sum = +val.toString().slice(0, j - d) + +val.toString().slice(j - d);
    // console.log(
    //   val.toString().slice(0, j - d) + " " + val.toString().slice(j - d)
    // );
    return sum == i;
  }
  return i == 1;
}

console.log(kaprekarNumbers(400, 700));
