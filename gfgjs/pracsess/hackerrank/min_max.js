class Solution {
  //Function to return max value that can be put in knapsack of capacity W.
  solveMeFirst(k, arr) {
    arr.sort((a, b) => a - b);
    let n = arr.length
    let val = arr[n-1]
    for(let i=0;i<n-k+1;i++){
       Math.min(arr[i+k-1]-arr[i])
        if(Math.min(arr[i+k-1]-arr[i])<val){
            val = Math.min(arr[i+k-1]-arr[i])
        }
    }
    return val;
  }
}

var soln = new Solution();

console.log(
  soln.solveMeFirst(
    3,
    [
    10,100,300,200,1000,20,30
    ]
  )
//   soln.solveMeFirst(
//     5,
//     [
//       4504, 1520, 5857, 4094, 4157, 3902, 822, 6643, 2422, 7288, 8245, 9948,
//       2822, 1784, 7802, 3142, 9739, 5629, 5413, 7232,
//     ]
//   )
);
