
class Solution {

    numberOfPaths(m,n){
        //code here
        if(m==1 || n==1){
            return 1
         }
         return this.numberOfPaths(m-1,n) + this.numberOfPaths(m, n-1)
    }

}


var soln = new Solution();

  console.log(soln.numberOfPaths(3,3));