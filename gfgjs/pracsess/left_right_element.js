class Solution {
  findElement(arr, n) {
    let result = -1;
    let i = 0;
    let p = false;
    let big = arr[0];
    while (i < n - 1) {
      if (arr[i] >= big) {
        big = arr[i];
        if (p) result = arr[i];
        p = false;
      } else {
        p = true;
        result = -1;
      }
      i++;
    }
    if (arr[n - 1] < big) result = -1;
    return result;
  }
}

var soln = new Solution();

console.log(soln.findElement([4, 2, 5, 7], 4));
