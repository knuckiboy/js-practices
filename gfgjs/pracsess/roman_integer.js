class Solution {
  romanToDecimal(str) {
    //code here
    let result = 0;
    let i = str.length - 1;
    let c=0;
    let n=0;
    while (i >= 0) {
      if (String(str).charAt(i) === "I") {
        n = 1;
      }
      if (String(str).charAt(i) === "V") {
        n = 5;
      }
      if (String(str).charAt(i) === "X") {
        n = 10;
      }
      if (String(str).charAt(i) === "L") {
        n = 50;
      }
      if (String(str).charAt(i) === "C") {
        n = 100;
      }
      if (String(str).charAt(i) === "D") {
        n = 500;
      }
      if (String(str).charAt(i) === "M") {
        n = 1000;
      }
      if (c <= n) {
        c = n;
        result = result + n;
      } else {
        c = n;
        result = result - n;
      }
      i--;
    }
    return result;
  }
}

var soln = new Solution();

//   soln.maxChainLen([[5, 24], [39, 60], [15, 28], [27, 40], [50, 90]], 5)
console.log(soln.romanToDecimal("CMXVI"));
