

class Solution{
    printLargest(arr){
        //code here
        arr.sort(function(a, b){ return (String(b)+String(a)) - (String(a)+String(b))})
        return Array.from(arr).join("")
    }

}

var soln = new Solution();

console.log(soln.printLargest([54, 546, 548, 60]));
// soln.printLargest([3, 30, 34, 5, 9]);