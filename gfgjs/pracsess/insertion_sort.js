class Solution {
  solution(A) {
    let i = 1,
      j = 0,
      key = 0;

    while (i < A.length) {
      j = i - 1;
      key = A[i];
      while (j >= 0 && A[j] >= key) {
        A[j + 1] = A[j];
        j--;
    }
    A[j+1] = key;
      i++;
    }
    return A;
  }
}

var soln = new Solution();

console.log(soln.solution([4, 3, 2, 10, 12, 1, 5, 6]));
