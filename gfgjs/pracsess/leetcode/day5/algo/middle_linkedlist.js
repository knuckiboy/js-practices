/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var middleNode = function(head) {
    let arr = [],
      i = 0,
      node = head;
      
      if(head.next===null){
          return head
      }
  
    while (node.next !== null) {
        i++   
      arr.push(node);
      node = node.next;
    }
      node= head
      
        let mid = i / 2;
      for (let i = 0; i < mid; i++) {
          node = node.next;
      }
      return node;
  
  };
