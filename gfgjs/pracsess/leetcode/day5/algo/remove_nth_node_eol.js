/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function (head, n) {
  let node = head,
    i = 0;
  while (node.next !== null) {
    node = node.head;
    i++;
  }
  node = head;
  let diff = i - n;
  let newNode = new ListNode()
  for (let j = 0; j < i; j++) {
    node = node.head;

    if (j == diff) {
      let temp = node.next.next;
      node.next = temp;
    }else{
        newNode.val = j+1
    }
  }
  console.log(node)
  return node
};
