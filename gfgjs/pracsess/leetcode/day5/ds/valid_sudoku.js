/**
 * @param {character[][]} board
 * @return {boolean}
 */
var isValidSudoku = function (board) {
  for (let i = 0; i < board[0].length; i++) {
    let arr = Array.from(Array(9), (_, index) => index + 1);
    let arr2 = [...arr];
    for (let j = 0; j < board[0].length; j++) {
      if (board[i][j] != ".") {
        //   console.log(board[i][j])
        if (arr[board[i][j] - 1] != -1) {
          arr[board[i][j] - 1] = -1;
        } else return false;
      }
      if (board[j][i] != ".") {
        if (arr2[board[j][i] - 1] != -1) {
          arr2[board[j][i] - 1] = -1;
        } else return false;
      }
    }
  }
  return true;
};

console.log(
  isValidSudoku([
    ["5", "3", ".", ".", "7", ".", ".", ".", "."],
    ["6", ".", ".", "1", "9", "5", ".", ".", "."],
    [".", "9", "8", ".", ".", ".", ".", "6", "."],
    ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
    ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
    ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
    [".", "6", ".", ".", ".", ".", "2", "8", "."],
    [".", ".", ".", "4", "1", "9", ".", ".", "5"],
    [".", ".", ".", ".", "8", ".", ".", "7", "9"],
  ])
);
