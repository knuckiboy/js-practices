/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
 var searchMatrix = function(matrix, target) {
     let row = matrix.length, col = matrix[0].length
    for(let i=0;i<row;i++){
        for(let j=0;j<col;j++){
            console.log(matrix[i][j])
            if(matrix[i][j]===target){
                return true
            }
        }
    }
    return false
};

console.log(searchMatrix([[1,3,5,7],[10,11,16,20],[23,30,34,60]],13))