/**
 * @param {number[]} prices
 * @return {number}
 * take note
 */
var maxProfit = function (prices) {
  if (prices.length < 2) return 0;

  max = 0;
  min = prices[0];
  for (p of prices) {
    //   console.log(p)
    //   console.log(min)
    max = Math.max(max, p - min);
    min = Math.min(min, p);
  }
  return max;
};

console.log(maxProfit([7,1,5,3,6,4]));
