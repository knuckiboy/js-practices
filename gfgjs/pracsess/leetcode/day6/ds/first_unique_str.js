/**
 * @param {string} s
 * @return {number}
 */
var firstUniqChar = function (s) {
  let m = new Map();
  let min = Number.MAX_SAFE_INTEGER;
  for (let j = 0; j < s.length; j++) {
    if (m.has(s[j])) {
      m.set(s[j], m.get(s[j]) + 1);
    } else {
      m.set(s[j], 1);
    }
  }
//   console.log(m)
  for (let [k, v] of m.entries()) {

    if (v == 1) {
      min = Math.min(s.indexOf(k), min);
    }
  }
  return min == Number.MAX_SAFE_INTEGER ? -1 : min;
};

console.log(firstUniqChar("aabb"))