/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function (s, t) {
  if (s.length != t.length) return false;
  let a = s,
    b = t;
  a = a.split("").sort();
  b = b.split("").sort();
  return a.every((e, i) => e === b[i]);
};

console.log(isAnagram("rat", "car"));
