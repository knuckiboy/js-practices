/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var checkInclusion = function (s1, s2) {
  if (s2.includes(s1)) {
    return true;
  }
  let s3 = [...s1].reverse().join("");
  console.log(s3);
  if (s2.includes(s3)) {
    return true;
  }
  return false;
};

console.log(checkInclusion("ab", "eidbaooo"));
