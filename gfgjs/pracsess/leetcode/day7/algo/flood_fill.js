/**
 * @param {number[][]} image
 * @param {number} sr
 * @param {number} sc
 * @param {number} newColor
 * @return {number[][]}
 */

const dir = [
  [0, 1],
  [0, -1],
  [1, 0],
  [-1, 0],
];
var original;
var floodFill = function (image, sr, sc, newColor) {
  if (
    image == null ||
    image.length == 0 ||
    image[0].length == 0 ||
    image[sr][sc] == newColor
  )
    return image;
  original = image[sr][sc];
  dfs(image, sr, sc, newColor);

  return image;
};

var dfs = function (image, sr, sc, newColor) {
  image[sr][sc] = newColor;
  console.log(original)
  let tx = 0,
    ty = 0;
  for (let i = 0; i < 4; i++) {
    tx = sr + dir[i][0];
    ty = sc + dir[i][1];
    if (
      tx >= 0 &&
      tx < image.length &&
      ty >= 0 &&
      ty < image[0].length &&
      image[tx][ty] == original
    ) {
      dfs(image, tx, ty, newColor);
    }
  }
};

// console.log(
//   floodFill(
//     [
//       [1, 1, 1],
//       [1, 1, 0],
//       [1, 0, 1],
//     ],
//     1,
//     1,
//     2
//   )
// );
console.log(
  floodFill(
    [[1,1,1],[1,1,0],[1,0,1]],
    1,
    1,
    2
  )
);
