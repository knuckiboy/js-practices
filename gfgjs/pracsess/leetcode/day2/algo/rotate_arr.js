/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var rotate = function (nums, k) {
  let j = k % nums.length;
  let i = 0,
    arr = [];
  for (let b = 0; b < j; b++) {
    arr[b] = nums[nums.length - j + b];
  }

  for (let a = nums.length - 1; a >= j; a--) {
    nums[a] = nums[a - j];
  }
  for (let b = 0; b < arr.length; b++) {
    nums[b] = arr[b];
  }
  return nums;
};

console.log(rotate([1, 2, 3, 4, 5, 6, 7], 3));
