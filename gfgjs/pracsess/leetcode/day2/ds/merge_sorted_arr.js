/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 * take note
 */
// var merge = function (nums1, m, nums2, n) {
//   while (m > 0 && n > 0) {
//     if (nums1[m-1] > nums2[n-1]) {
//       nums1[m + n - 1] = nums1[m-1];
//       m--;
//     } else {
//       nums1[m + n - 1] = nums2[n-1];
//       n--;
//     }
//   }

//   while(n > 0){
//     nums1[m+n-1] = nums2[n-1];
//     n--;
// }

//   return nums1;
// };

// console.log(merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3));

// 

function find_mistake(nums) {
    nums.sort((a,b)=> a-b)
    let arr = Array(nums.length).fill(0).map((a,i)=> i+1)
    for(let i=0;i<nums.length;i++){
      if(arr[i]!=nums[i]){
        return arr[i]+nums[i]
      }
    }
    
  }

  console.log(find_mistake([6,3,2,4,3,1]))