/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 * 2 pointer question
 */
var twoSum = function (nums, target) {
  let l = 0,
    r = nums.length - 1;
  let arr = [...nums];
  arr.sort((a, b) => a - b);
  while (l <= r) {
    // console.log(nums[l] + nums[r]);
    if (arr[l] + arr[r] == target) {
      var j = nums.indexOf(arr[l]);
      nums[j] = "a";
      var k = nums.indexOf(arr[r]);
      nums[k] = "a";
      return [j, k];
    }
    if (arr[l] + arr[r] > target) {
      r--;
    } else {
      l++;
      r = nums.length - 1;
    }
  }
};

// console.log(twoSum([-1, -2, -3, -4, -5], -8));
console.log(twoSum([3, 3], 6));
