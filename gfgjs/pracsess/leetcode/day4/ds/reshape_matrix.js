/**
 * @param {number[][]} mat
 * @param {number} r
 * @param {number} c
 * @return {number[][]}
 */
var matrixReshape = function (mat, r, c) {
  if (r * c !== mat[0].length * mat.length) {
    return mat;
  }
  let k = 0,
    h = 0,
    arr = [];
  for (let i = 0; i < r; i++) {
    let temp = [];
    for (let j = 0; j < c; j++) {
      temp.push(mat[h][k]);
      if (k < mat[0].length) {
        k++;
      }
      if (k == mat[0].length) {
        h++;
        k = 0;
      }
    }
    arr.push(temp);
  }
  return arr;
};

console.log(
  matrixReshape(
    [
      [1, 2],
      [3, 4],
    ],
    1,
    4
  )
);
console.log(
  matrixReshape(
    [
      [1, 2],
      [3, 4],
    ],
    2,
    4
  )
);
