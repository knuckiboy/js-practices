/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function (numRows) {
  let arr = [[1]];
  for (let i = 1; i < numRows; i++) {
    let temp = [];
    temp.push(1);
    for (let j = 1; j < i; j++) {
      temp.push(arr[i-1][j - 1]  + arr[i-1][j]);
    }
    temp.push(1);
    // console.log(temp);
    arr.push(temp);
  }
  return arr;
};

console.log(generate(1));
