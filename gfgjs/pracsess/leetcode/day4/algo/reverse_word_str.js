/**
 * @param {string} s
 * @return {string}
 */
var reverseWords = function (s) {
  let newStr = "";
  let arr = s.split(" ");
  for (let i = 0; i < arr.length; i++) {
    let a = arr[i].split("");
    strReverse(a, 0, a.length - 1);
    if (i < arr.length - 1) {
      newStr += a.join("") + " ";
    } else {
      newStr += a.join("");
    }
  }
  return newStr;
};

var strReverse = function (s, l, r) {
  if (l >= r) return;
  let temp = s[l];
  s[l] = s[r];
  s[r] = temp;
  strReverse(s, l + 1, r - 1);
};

console.log(reverseWords("Let's take LeetCode contest"));
