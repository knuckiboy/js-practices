/**
 * @param {character[]} s
 * @return {void} Do not return anything, modify s in-place instead.
 */
 var reverseString = function(s) {
        let i=0;
        while(i<s.length/2){
            let temp = s[i]
            s[i] = s[s.length-i-1]
            s[s.length-i-1] = temp
            i++
        }
        return s
};

console.log(reverseString(["A"," ","m","a","n",","," ","a"," ","p","l","a","n",","," ","a"," ","c","a","n","a","l",":"," ","P","a","n","a","m","a"]))