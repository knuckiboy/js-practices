/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function (nums) {
  let max = Number.NEGATIVE_INFINITY;
  let arr= new Array(nums.length)
  for (let i = 0; i < nums.length; i++) {
      if(i==0){
          arr[0]=nums[0]
      }else{
          arr[i] = Math.max(nums[i], arr[i-1]+ nums[i])
      }
      max = Math.max(
        max, arr[i]
      );

    }
  return max
};

console.log(maxSubArray([1,2,-1,-2,2,1,-2,1,4,-5,4]))
