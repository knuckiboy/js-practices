/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var searchInsert = function (nums, target) {
  let l = 0,
    r = nums.length - 1;
  while (l <= r) {
    let mid = Math.floor(l + (r - l) / 2);
    console.log(mid)
    if (mid == 0 && nums[mid] >= target) return mid;
    if (mid == nums.length - 1 && nums[mid] >= target) return mid;
    if (mid == nums.length - 1 && nums[mid] < target) return mid + 1;
    if (nums[mid] >= target && nums[mid - 1] < target) return mid;
    if (nums[mid] > target && nums[mid - 1] >= target) {
      r = mid - 1;
    } else {
      l = mid + 1;
    }
  }
};

console.log(searchInsert([1], 2));
