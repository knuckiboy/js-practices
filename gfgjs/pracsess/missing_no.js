class Solution {
  findSum(n) {
    if (n == 1) {
      return 1 ;
    }
    return n+this.findSum(n - 1);
  }

  MissingNumber(array, n) {
    //code here
   let sum = this.findSum(n);

   let sum2 = 0
    let i=0;
   while(i<n-1){
       sum2+=array[i]
       i++
   }

   return sum-sum2;

  }
}

var soln = new Solution();

console.log(soln.MissingNumber([6,1,2,8,3,4,7,10,5], 10));
